/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package endevinabugs;



import java.util.Scanner;
import java.util.Random;

/**
 * La classe EndevinaBugs és una aplicació que utilitza un objecte Random per a
 * jugar a endevinar un número entre un jugador humà i l'ordinador.
 *
 * Per arreglar l'error només cal fer canvis MENORS!!
 *
 */
public class EndevinaBugs {

    // Podeu suposar que tots els comentaris són correctes
    public static void main(String[] args) {

        //Aquestes declaracions són correctes
        Random ranGen = new Random(8);	// posem una llavor per fer el debugging més fàcil
        final int cares = 6;			// nombre de cares del dau
        Scanner in = new Scanner(System.in);
        int estimacioJugador = -1;				// estimació del jugador
        int tiradaDau = -1;				// tirada de dau
        int puntsOrdinador = 0;			// puntuació de l'ordinador, 0 - 5 max
        int puntsJugador = 0;			// puntuació del jugador, 0 - 5 max
        boolean esIntentCorrecte = false;		// boolea per indicar si s'ha encertat
        int numeroIntents = 0;				// conta el nombre d'intents per ronda

        //  ASSEGURA'T QUE EL PROGRAMA JUGA AMB AQUESTES REGLES!!!
        System.out.println("Benvingut al joc ENDEVINA EL DAU!\n\n REGLES:");
        System.out.println("1. Es jugaran 5 rondes.");
        System.out.println("2. A cada ronda hauràs d'endevinar el número aparegut en un dau de sis cares.");
        System.out.println("3. Si endevines el número aparegut en tres intents o menys\n"
                + "   guanyes un punt, en cas contrari, jo guanyo un punt.");
        System.out.println("4. Qui tingui més punts després de 5 rondes guanyarà el joc.");

        // juguem 5 rondes
        for (int ronda = 0; ronda < 5; ronda++) {
//BUG: si le dejamos el igual al empezar en 0 se juegan 6 rondas
            // tirem el dau al principi de la ronda
            numeroIntents = 0;
            //BUG: siempre que empieza el for hay que inicializar la variable si no
            //cuando se llega a los 3 intentos siempre tiene valor 3
            System.out.println("\n\nRONDA " + ronda);
            System.out.println("-------");
            tiradaDau = ranGen.nextInt(cares) + 1;
            //BUG: nos puede dar un 0 por que le estamos diciendo que nos de un numero 
            //entre 0 y 7 sin llegar a 7, el +1 va fuera del parentesis
            System.out.println("L'ordinador ha tirat el dau.");
            System.out.println("Tens tres intents.");

            // el següent bucle gestiona els tres intents
            esIntentCorrecte = false;

            while (numeroIntents < 3 && esIntentCorrecte == false) {
                //BUG: el boolean tiene que ser false para que entre en el while
                // !esIntentCorrecte no lo acepta

                // input i validació: ha de ser un valor entre 1 i 6 inclosos
                do {
                    System.out.print("\nQuina tirada he tret [1-6]? ");

                    estimacioJugador = in.nextInt();

                    if ((estimacioJugador < 1) || (estimacioJugador > 6)) {
                        System.out.println("   Siusplau, entra un valor vàlid [1-6]!");
                    }
                    //BUG: no da este error por que con && siempre se cumple alguna de las condiciones
                } while (estimacioJugador < 1 || estimacioJugador > 6);

                // el jugador ha encertat?
                if (tiradaDau == estimacioJugador) {
                    System.out.println("   Correcte!");
                    esIntentCorrecte = true;
                    //BUG: tenemos que cambiar el boolean para que salga del while
                } else {
                    System.out.println("   Aquesta no era la tirada.");
                    numeroIntents++;
                    //BUG: solo tiene que sumar numero de intentos cuando falla
                }
                // numeroIntents++;
            }

            // si el jugador ha encertat, guanya un punt
            // en cas contrari l'ordinador rep un punt
            if (esIntentCorrecte) {
                puntsJugador++;
            } else {

                puntsOrdinador++;
            }
//BUG: cambiamos la condicion del if para que si esIntentCorrecte es true le de un punto a jugador
            // mostra la resposta i les puntuacions
            System.out.println("\n*** La resposta correcta era: " + tiradaDau + " ***\n");
            System.out.println("Punts:");
            System.out.println("  Tu: \t\t" + puntsJugador);
            System.out.println("  Ordinador: \t" + puntsOrdinador);
            System.out.println("");
        }

        // dir al jugador si ha guanyat o ha perdut
        if (puntsOrdinador > puntsJugador) {
            System.out.println("*** Has perdut! ***");
        } else {
            System.out.println("*** Has guanyat! ***");
        }

        System.out.println("Gràcies per jugar a endevina el dau!");
    }
}